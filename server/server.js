import express from "express";
import { corsOptions } from "./configs/serverConfig.js";
import cors from "cors";
import {
  getFileById,
  getSubtitleById,
  getThumbnailById,
  getVideoInfoById,
  updateSubtitleById,
  updateTagById,
} from "./controllers/videoController.js";
import {
  getAllErrorListings,
  getListings,
  updateAllListings,
} from "./controllers/listController.js";
import dotenv from "dotenv";
import { patchLikeById } from "./controllers/likeController.js";
import {
  deleteSetting,
  getAllSettings,
  updateSettings,
} from "./controllers/settingController.js";

dotenv.config({ path: ".env" });

const PORT = process.env.SERVER_PORT || 3000;
const app = express();

//enable cross origin resource sharing
app.use(cors(corsOptions));
//middleware to allow express to work with additional datatypes
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Settings Routes
app.get("/api/setting", getAllSettings);
app.post("/api/setting", updateSettings);
app.delete("/api/setting/:databaseId", deleteSetting);

// Video Routes
app.get("/api/video/file/:databaseId/:id/:videoId", getFileById);
app.get("/api/video/subtitle/:databaseId/:id/:videoId", getSubtitleById);
app.get("/api/video/thumbnail/:databaseId/:id", getThumbnailById);
app.get("/api/video/info/:databaseId/:id", getVideoInfoById);
app.post("/api/video/subtitle/:databaseId/:id", updateSubtitleById);
app.post("/api/video/tag/:databaseId/:id", updateTagById);

app.patch("/api/video/like/:databaseId/:uuid", patchLikeById);

// Listing Routes
app.get("/api/video/list/:databaseId", getListings);
app.post("/api/video/list/database/:databaseId", updateAllListings);
app.get("/api/video/error/:databaseId", getAllErrorListings);

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
