import axios from "axios";
import { parse } from "node-html-parser";
import * as fsPromise from "node:fs/promises";
import fs from "fs";
import path from "path";

export const scrapeTags = async (title) => {
  try {
    const tagWebsite = process.env.TAG_WEBSITE;
    const tags = [];
    // process title to fit into scraping website
    const url = `${tagWebsite}/torrent/${String(title)
      .replaceAll("-", "")
      .toLowerCase()}`;

    // Scrape from TAG_WEBSITE
    const response = await axios.get(url);
    // parse and retrieve tags from site
    const rootPage = parse(String(response.data));
    const html_tags = rootPage.querySelectorAll(".tag, .is-light");

    for (const index in html_tags) {
      tags.push(String(html_tags[index].rawText).trim());
    }
    return tags.length === 0 ? ["NA"] : tags;
  } catch (err) {
    // console.log(err);
    throw new Error(`Unable to scrape tags from site`);
  }
};

export const scrapeSubtitleFile = async (title = "", filepath = "") => {
  try {
    const destSubtitleFilePath = `${getSrtFilePath(filepath)}`;
    const SUBTITLE_WEBSITE = process.env.SUBTITLE_WEBSITE;

    if (fs.existsSync(getVttFilePath(filepath)))
      throw new Error(`Skipping, file exists in ${getVttFilePath(filepath)}`);

    // scrape search result html page to get subtitle download urls
    const response = await axios.get(
      `${SUBTITLE_WEBSITE}/subs/1/${title}.html`
    );

    // parse and retrieve subtitle download for english
    const rootPage = parse(String(response.data));
    const download_en_tag = rootPage.querySelector("#download_en");

    if (download_en_tag === null)
      throw new Error(
        `Unable scrape subtitles from ${SUBTITLE_WEBSITE} for download link of file title: ${title}`
      );

    const { href = "" } = download_en_tag.attributes;

    // download subtitle from web scrape
    const subtitleFilePath = await downloadFile(
      `${SUBTITLE_WEBSITE}${href}`,
      destSubtitleFilePath
    );

    // convert downloaded srt into vtt
    const vttFilePath = await createVttFromSrt(subtitleFilePath);

    return vttFilePath;
  } catch (err) {
    throw err;
  }
};

function getVttFilePath(filepath = "") {
  return `${path.parse(filepath).dir}\\${path.parse(filepath).name}.vtt`;
}
function getSrtFilePath(filepath = "") {
  return `${path.parse(filepath).dir}\\${path.parse(filepath).name}.srt`;
}

export async function downloadFile(url, subtitleFilePath) {
  try {
    const filename = path.parse(subtitleFilePath).name;
    const filedir = path.parse(subtitleFilePath).dir;
    const subtitleExt = "srt";
    const tmpSubtitleFilePath = `${filedir}\\${filename}.${subtitleExt}.tmp`;
    const response = await axios({
      url,
      method: "GET",
      responseType: "stream",
    });

    await fsPromise.writeFile(`${tmpSubtitleFilePath}`, response.data);
    if (await isSubtitleFileValid(tmpSubtitleFilePath)) {
      await fsPromise.rename(`${tmpSubtitleFilePath}`, `${subtitleFilePath}`);
    } else {
      // throw error if subtitle downloaded exceeds allowable threshold of non-valid utf-8 characters
      await fsPromise.rm(tmpSubtitleFilePath);
      throw new Error(
        "Subtitle found but rejected due to invalid char threshold failure"
      );
    }

    return subtitleFilePath;
  } catch (err) {
    if (err?.response?.status === 404) {
      throw new Error("Subtitle not found from site");
    } else {
      throw err;
    }
  }
}

async function isSubtitleFileValid(filepath) {
  const data = await fsPromise.readFile(filepath, { encoding: "utf-8" });
  const threshold = 0.6; // 70% cut off if invalid
  let totalCharCount = 0; // total char counted
  let validCharCount = 0; // total valid char

  fs.readFileSync(filepath, "utf-8")
    .split(/\r?\n/)
    .forEach(function (line, index) {
      if (!line.includes("-->") && isNaN(line)) {
        totalCharCount += line.length;
        for (let i = 0; i < line.length; i++) {
          if (line.charCodeAt(i) <= 127) {
            validCharCount++;
          }
        }
      }
    });

  if (validCharCount / totalCharCount >= threshold) return true;

  return false;
}

export async function createVttFromSrt(srtFilePath) {
  try {
    const vttFilePath = `${getVttFilePath(srtFilePath)}`;
    const tmpVttFilePath = `${vttFilePath}.tmp`;
    const contents = await fsPromise.readFile(srtFilePath, "utf-8");

    await fsPromise.writeFile(tmpVttFilePath, srt2webvtt(contents), "utf-8");

    await fsPromise.rename(`${tmpVttFilePath}`, `${vttFilePath}`);

    return vttFilePath;
  } catch (err) {
    return err;
  }
}

function srt2webvtt(data = "") {
  try {
    // remove dos newlines
    let srt = String(data).replace(/\r+/g, "");
    // trim white space start and end
    srt = srt.replace(/^\s+|\s+$/g, "");
    // get cues
    let cuelist = srt.split("\n\n");
    let result = "";
    if (cuelist.length > 0) {
      result += "WEBVTT\n\n";
      for (let i = 0; i < cuelist.length; i = i + 1) {
        // console.log(`i:${i}`, cuelist[i], convertSrtCue(cuelist[i]));
        result += convertSrtCue(cuelist[i]);
      }
    }
    return result;
  } catch (err) {
    console.log(err);
  }
}
function convertSrtCue(caption) {
  // remove all html tags for security reasons
  //srt = srt.replace(/<[a-zA-Z\/][^>]*>/g, '');
  let cue = "";
  let s = caption.split(/\n/);
  // concatenate muilt-line string separated in array into one
  while (s.length > 3) {
    for (let i = 3; i < s.length; i++) {
      s[2] += "\n" + s[i];
    }
    s.splice(3, s.length - 3);
  }
  let line = 0;
  // detect identifier
  if (s[1] === undefined)
    console.log(`detected undefined in srtcue`); // error patched
  else if (!s[0].match(/\d+:\d+:\d+/) && s[1].match(/\d+:\d+:\d+/)) {
    cue += s[0].match(/\w+/) + "\n";
    line += 1;
  }
  // get time strings
  if (s[line].match(/\d+:\d+:\d+/)) {
    // convert time string
    let m = s[1].match(
      /(\d+):(\d+):(\d+)(?:,(\d+))?\s*--?>\s*(\d+):(\d+):(\d+)(?:,(\d+))?/
    );
    if (m) {
      cue +=
        m[1] +
        ":" +
        m[2] +
        ":" +
        m[3] +
        "." +
        m[4] +
        " --> " +
        m[5] +
        ":" +
        m[6] +
        ":" +
        m[7] +
        "." +
        m[8] +
        "\n";
      line += 1;
    } else {
      // Unrecognized timestring
      return "";
    }
  } else {
    // file format error or comment lines
    return "";
  }
  // get cue text
  if (s[line]) {
    cue += s[line] + "\n\n";
  }
  return cue;
}
