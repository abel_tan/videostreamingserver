import * as fs from "fs/promises"; // allow promises
import HashMap from "hashmap";
import path from "path";
import getUuidByString from "uuid-by-string";

// export const ALL_VIDEO_FORMATS = [".mp4"];
// export const SUPPORTED_VIDEO_FORMATS = [".mp4"];
// export const ALL_IMG_FORMATS = [".png", ".jpg"];
// export const ALL_SUBTITLE_FORMATS = [".vtt", ".srt"];

export const create_video_map = async (dir = "") => {
  const paths = [];
  let videoHashMap = new HashMap();
  const filepathsStats = await directoryRead(dir, paths); // list all files
  videoHashMap = buildHashMap(videoHashMap, filepathsStats); // build hash map by directory name
  videoHashMap = buildMetadata(videoHashMap); // build metadata for each category
  return videoHashMap;
};

// Once metadata has been built like american sniper
async function buildMetadata(videoHashMap = new HashMap()) {
  const newVideoHashMap = new HashMap();

  for (const [key, filepathsStats] of videoHashMap.entries()) {
    const aggSupportedVideoPaths = [];
    const aggImagePaths = [];
    const aggErrPaths = [];
    const aggSubtitlePaths = [];
    const sequentialStats = [];

    // build into filepaths
    filepathsStats.map((filepathStats) => {
      const { filepath, size, birthtime } = filepathStats;
      let extname = path.extname(filepath) || "";

      // consolidate file stats
      sequentialStats.push({ size, birthtime });

      // catch error files
      if (extname === "") {
        aggErrPaths.push(filepath);
      } else if (process.env.SUPPORTED_VIDEO_FORMATS.includes(extname)) {
        // consolidate video metadata into arr
        aggSupportedVideoPaths.push(filepath);
      } else if (process.env.ALL_IMG_FORMATS.includes(extname)) {
        // consolidate images into arr
        aggImagePaths.push(filepath);
      } else if (process.env.SUPPORTED_SUBTITLE_FORMATS.includes(extname)) {
        // consolidate subtitles into arr
        aggSubtitlePaths.push(filepath);
      } else {
        // consolidate filepaths not processed properly into arr
        aggErrPaths.push(filepath);
      }
    });

    // tidy up video paths metadata
    const files = aggSupportedVideoPaths.map((vp, index) => {
      return {
        videoId: index + 1,
        filepath: vp,
        subtitlePath: getRecommendedSubtitle(aggSubtitlePaths, vp),
        ext: path.extname(vp).slice(1),
        size: sequentialStats[index].size,
        birthtime: sequentialStats[index].birthtime,
      };
    });

    // consolidate into expected json format
    newVideoHashMap.set(key, {
      title: recommendTitle(filepathsStats[0]?.filepath || ""),
      thumbnailPath: aggImagePaths[0] || "",
      files,
      invalidPaths: aggErrPaths,
      label: recommendLabel(filepathsStats[0]?.filepath || ""),
      oldestBirthDate: getOldestBirthDate(sequentialStats),
      uuid: getUuidByString(getParentBasename(filepathsStats[0]?.filepath)),
      subtitleExists: aggSubtitlePaths.length > 0,
    });
  }

  return newVideoHashMap;
}

function getOldestBirthDate(fileStats = []) {
  return fileStats.sort((a, b) => a.birthtime - b.birthtime)[0].birthtime;
}

// Match subtitle name to video name to get recommendation
function getRecommendedSubtitle(subtitlePaths = [], videoPath = "") {
  return subtitlePaths.find(
    (sp) =>
      path.parse(path.basename(sp)).name ===
      path.parse(path.basename(videoPath)).name
  );
}

function recommendLabel(filepath = "") {
  const parentFolderName = getParentBasename(filepath);
  if (parentFolderName.includes("-")) {
    return parentFolderName.split("-")[0].trim();
  } else if (parentFolderName.includes(" ")) {
    return parentFolderName.split(" ")[0].trim();
  } else {
    return parentFolderName;
  }
}

function recommendTitle(filepath = "") {
  return getParentBasename(filepath).trim();
}

// walk all directories and get files w stats
async function directoryRead(dir, filelist = []) {
  const files = await fs.readdir(dir);

  for (let file of files) {
    const filepath = path.join(dir, file);
    const stat = await fs.stat(filepath);

    if (stat.isDirectory()) {
      filelist = await directoryRead(filepath, filelist);
    } else {
      filelist.push({ filepath: path.join(dir, file), ...stat });
    }
  }

  return filelist;
}

// Categorize filepaths into buckets by parent directory name (using generated uuid)
const buildHashMap = (videoHashMap = new HashMap(), filepathsStats = []) => {
  filepathsStats.map((filepathStats) => {
    const parentFolderName = getUuidByString(
      getParentBasename(filepathStats.filepath)
    );
    updateHashMapEntry(videoHashMap, parentFolderName, filepathStats);
  });
  return videoHashMap;
};

// return parent folder name
export const getParentBasename = (filepath = "") => {
  return path.basename(path.dirname(filepath));
};

const updateHashMapEntry = (videoHashMap = new HashMap(), key, value) => {
  if (videoHashMap.get(key) === undefined) {
    videoHashMap.set(key, [value]);
  } else {
    videoHashMap.set(key, videoHashMap.get(key).concat(value));
  }
};
