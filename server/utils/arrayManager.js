export const mergeArraysById = (
  firstArr = [],
  secondArr = [],
  joinById = ""
) => {
  return firstArr.map((t1) => ({
    ...t1,
    ...secondArr.find((t2) => String(t2[joinById]) === String(t1[joinById])),
  }));
};
