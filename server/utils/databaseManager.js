import { getDatabaseConn } from "../configs/databaseConfig.js";
import { DATABASE_TYPE_ACTION } from "../constants/index.js";

export async function updateStoredVideo(databaseId, data) {
  const db = await getDatabaseConn(databaseId);
  const { videos } = db.data;
  const index = videos.findIndex((v) => String(v.id) === String(data.id));
  videos[index] = data;
  await db.write();
}

export async function getStoredVideoById(databaseId, id = -1, videoId = -1) {
  const db = await getDatabaseConn(databaseId);
  const { videos = [] } = db.data;

  // two options of filtering
  if (videoId === -1) {
    return Array.from(videos).find((v) => String(v.id) === String(id));
  } else {
    return Array.from(videos)
      .find((v) => String(v.id) === String(id))
      .files.find((f) => String(f.videoId) === String(videoId));
  }
}

export async function getStoredActionsByUuid(databaseId, uuid = "") {
  const db = await getDatabaseConn(databaseId, DATABASE_TYPE_ACTION);
  const { actions = [] } = db.data;

  return actions.find((l) => String(l.uuid) === String(uuid));
}

export async function updateStoredAction(databaseId, uuid, data) {
  const db = await getDatabaseConn(databaseId, DATABASE_TYPE_ACTION);

  // initialize if first time
  if (db.data.actions === undefined) {
    db.data.actions = [];
  }

  const index = db.data.actions.findIndex((v) => v.uuid === uuid);
  if (index === -1) {
    db.data.actions.push({ ...data, uuid });
  } else {
    db.data.actions[index] = { ...db.data.actions[index], ...data };
  }

  await db.write();
}
