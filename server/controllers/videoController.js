import fs from "fs";
import path from "path";
// import toWebVTT from "srt-webvtt";
import {
  getStoredActionsByUuid,
  getStoredVideoById,
  updateStoredAction,
  updateStoredVideo,
} from "../utils/databaseManager.js";
import { scrapeSubtitleFile, scrapeTags } from "../utils/downloader.js";
import { create_video_map } from "../utils/directoryParser.js";
import dayjs from "dayjs";

// @desc    POST video tag to trigger download for tag
// @route   /api/video/tag/:databaseId/:id
// @access  Public
export const updateTagById = async (req, res, next) => {
  try {
    const { databaseId, id } = req.params;
    const { uuid, title = "" } = await getStoredVideoById(databaseId, id);

    // Scrape from TAG_WEBSITE
    // upsert into videos database with tags
    const tags = await scrapeTags(title);
    await updateStoredAction(databaseId, uuid, {
      isTagDownloadFailed: false,
      tags,
    });

    res.status(200).send("Success");
  } catch (err) {
    const { databaseId, id } = req.params;
    const { uuid, title = "" } = await getStoredVideoById(databaseId, id);

    await updateStoredAction(databaseId, uuid, {
      isTagDownloadFailed: true,
      tagDownloadFailedDate: dayjs().format("DD/MM/YYYY"),
    });

    console.log(err);
    res.status(500).send("not found");
  }
};

// @desc    POST video subtitle to trigger download for subtitle
// @route   /api/video/subtitle/:databaseId/:id
// @access  Public
export const updateSubtitleById = async (req, res, next) => {
  try {
    const { databaseId, id } = req.params;
    console.log(`updateSubtitle`, databaseId, id);
    const {
      files = [],
      title = "",
      uuid = "",
    } = (await getStoredVideoById(databaseId, id)) || {};

    // only allow updating video subtitle for a single file, otherwise wont be able to scrape from website
    if (files.length > 1)
      throw new Error("Invalid, more than 1 valid video filepath detected");

    // scrape from website and download subtitle
    await scrapeSubtitleFile(title, files[0].filepath);

    // update entire metadata from directory, to insert id based on previously generated version
    const h = await create_video_map(path.parse(files[0].filepath).dir);
    for (const [key, metadata] of h.entries()) {
      // To update into database
      updateStoredVideo(databaseId, { id, ...metadata });
    }

    res.status(200).send("done");
  } catch (err) {
    // patch action database as download error
    const { databaseId, id } = req.params;
    const { uuid = "" } = (await getStoredVideoById(databaseId, id)) || {};
    updateStoredAction(databaseId, uuid, {
      isSubtitleDownloadFailed: true,
      subtitleDownloadFailedDate: dayjs().format("DD/MM/YYYY"),
    });

    res.status(500).send(err.message);
  }
};

// @desc    GET video info by id
// @route   /api/video/info/:databaseId/:id
// @access  Public
export const getVideoInfoById = async (req, res, next) => {
  const { databaseId, id } = req.params;
  const info = await getStoredVideoById(databaseId, id);
  const actionsObj = await getStoredActionsByUuid(databaseId, info?.uuid || {});

  if (info) {
    // enrich with api endpoints
    const thumbnailApi = `/api/video/thumbnail/${databaseId}/${id}`;
    info.files = info.files.map((f) => {
      return {
        ...f,
        subtitleApi: `/api/video/subtitle/${databaseId}/${id}/${f.videoId}`,
        videoApi: `/api/video/file/${databaseId}/${id}/${f.videoId}`,
      };
    });

    res.status(200).send({ ...info, thumbnailApi, ...actionsObj });
    return;
  } else {
    res.status(404).send("not found");
    return;
  }
};

// @desc    GET thumbnail by id
// @route   /api/video/thumbnail/:databaseId/:id
// @access  Public
export const getThumbnailById = async (req, res, next) => {
  const { databaseId, id = -1 } = req.params;
  const { thumbnailPath = "" } =
    (await getStoredVideoById(databaseId, id)) || {};

  // Thumbnail is missing
  if (!fs.existsSync(thumbnailPath)) {
    res.writeHead(200, {
      "Content-Type": "image/jpg",
    });
    fs.createReadStream("resources/img/thumbnail-not-found.jpg").pipe(res);
    return;
  }

  // Extracting file extension
  const ext = path.extname(thumbnailPath);

  // Setting default Content-Type
  let contentType = "text/plain";

  // Checking if the extension of
  // image is '.png'
  if (ext === ".png") contentType = "image/png";
  else if (ext === ".jpg") contentType = "image/jpg";

  // Setting the headers
  res.writeHead(200, {
    "Content-Type": contentType,
  });

  // Reading the file
  fs.createReadStream(thumbnailPath).pipe(res);
};

// @desc    GET subtitle by id
// @route   /api/video/subtitle/:databaseId/:id/:videoId
// @access  Public
export const getSubtitleById = async (req, res, next) => {
  const { databaseId, id, videoId } = req.params;
  console.log("came in getSubtitleById");
  const { subtitlePath = "" } = await getStoredVideoById(
    databaseId,
    id,
    videoId
  );

  if (fs.existsSync(subtitlePath)) {
    const subtitleFile = fs.readFileSync(subtitlePath, "utf8");
    res.status(200).send(subtitleFile);
  } else {
    res.status(200).send("");
  }
};

// @desc    GET video by id
// @route   /api/video/file/:databaseId/:id/:videoId
// @access  Public
export const getFileById = async (req, res, next) => {
  const { databaseId, id, videoId } = req.params;
  const { filepath = "" } = await getStoredVideoById(databaseId, id, videoId);

  // path.win32.
  if (!filepath || !fs.existsSync(filepath)) {
    return res.status(404).send("File not found");
  }

  const stat = fs.statSync(filepath);
  const fileSize = stat.size;
  const range = req.headers.range;

  if (range) {
    const parts = range.replace(/bytes=/, "").split("-");
    const start = parseInt(parts[0], 10);
    const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1;
    const chunksize = end - start + 1;
    const file = fs.createReadStream(filepath, { start, end });
    const head = {
      "Content-Range": `bytes ${start}-${end}/${fileSize}`,
      "Accept-Ranges": "bytes",
      "Content-Length": chunksize,
      "Content-Type": getFileContentType(filepath),
    };
    res.writeHead(206, head);
    file.pipe(res);
  } else {
    const head = {
      "Content-Length": fileSize,
      "Content-Type": getFileContentType(filepath),
    };
    res.writeHead(200, head);
    fs.createReadStream(filepath).pipe(res);
  }
};

function getFileContentType(filepath) {
  if (path.parse(filepath).ext === ".mp4") return "video/mp4";
  else return "video/mp4";
}
