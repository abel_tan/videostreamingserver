import { updateStoredAction } from "../utils/databaseManager.js";

// @desc    PATCH like by uuid (unique video parent folder name)
// @route   /api/video/like/:databaseId/:uuid
// @access  Public
export const patchLikeById = async (req, res, next) => {
  const { databaseId, uuid } = req.params;
  const data = req.body;

  console.log("came in patchLikeById", data);
  await updateStoredAction(databaseId, uuid, data);

  res.status(200).send(data);
};
