import fs from "fs";
import { getSettingDatabaseConn } from "../configs/databaseConfig.js";

// @desc    GET all settings
// @route   /api/setting
// @access  Public
export const getAllSettings = async (req, res, next) => {
  console.log(`getAllSettings`);
  const db = await getSettingDatabaseConn();
  res.status(200).json(db.data);
};

// @desc    POST update settings
// @route   /api/setting
// @access  Public
export const updateSettings = async (req, res, next) => {
  try {
    console.log(`updateSetting`);
    const data = req.body;

    const db = await getSettingDatabaseConn();
    db.data = data;

    console.log(`req.body`, req.body);

    const syncDatabase = (filepath) => {
      // create database if missing
      if (!fs.existsSync(filepath)) {
        const writeStream = fs.createWriteStream(filepath, {
          flags: "w",
        });

        writeStream.write("{}", "utf8", (err) => {
          if (err) {
            console.error("Error writing to file:", err);
          } else {
            console.log("Data has been written to the file.");
            writeStream.end();
          }
        });
      }
    };

    data.map((setting) => {
      const { databaseFilePath = "", databaseActionsFilePath = "" } = setting;
      // create database if missing
      syncDatabase(databaseFilePath);
      // create database if missing
      syncDatabase(databaseActionsFilePath);
    });

    db.write();
    res.status(200).json("done");
  } catch (err) {
    console.log(err);
  }
};

// @desc    DELETE setting
// @route   /api/setting/:databaseId
// @access  Public
export const deleteSetting = async (req, res, next) => {
  console.log(`deleteSetting`);
  const { databaseId = "" } = req.params;

  const db = await getSettingDatabaseConn();

  const index = db.data.findIndex(
    (setting) => String(setting.databaseId) === String(databaseId)
  );

  if (index !== -1) {
    // delete database
    const { databaseFilePath = "", databaseActionsFilePath = "" } =
      db.data[index];
    fs.existsSync(databaseFilePath) && fs.rmSync(databaseFilePath);
    fs.existsSync(databaseActionsFilePath) &&
      fs.rmSync(databaseActionsFilePath);

    // remove record
    db.data.splice(index, 1);
  }

  db.write();

  res.status(200).json(db.data);
};
