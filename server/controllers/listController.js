import {
  getDatabaseConn,
  getDatabaseSettingById,
} from "../configs/databaseConfig.js";
import { DATABASE_TYPE_ACTION } from "../constants/index.js";
import { mergeArraysById } from "../utils/arrayManager.js";
import { create_video_map } from "../utils/directoryParser.js";
import dayjs from "dayjs";

// @desc    GET videos by page
// @route   /api/video/list/:databaseId?videosPerPage&orderBy&searchCriteria
// @access  Public
export const getListings = async (req, res, next) => {
  const { databaseId } = req.params;
  const {
    videosPerPage = 10,
    orderedByAsc = true,
    searchCriteria = { title: "*", like: false, label: "*" },
  } = req.query;
  const decodedSearchCriteria = JSON.parse(decodeURIComponent(searchCriteria));
  console.log(`dec`, decodedSearchCriteria);

  const db = await getDatabaseConn(databaseId);
  const dbActions = await getDatabaseConn(databaseId, DATABASE_TYPE_ACTION);
  const { videos } = db.data;
  const { actions } = dbActions.data;
  const mergedList = mergeArraysById(videos, actions, "uuid");

  // Filter by search criteria
  const filteredList = filterSearchCriteria(mergedList, decodedSearchCriteria);

  // sort listing
  const sortedVideos = filteredList.sort(
    (a, b) => dayjs(b.oldestBirthDate) - dayjs(a.oldestBirthDate)
  );

  if (Boolean(orderedByAsc)) {
    res.status(200).json({
      videos: sortedVideos.slice(0, videosPerPage),
    });
    return;
  } else {
    res.status(200).json({
      videos: sortedVideos.reverse().slice(0, videosPerPage),
    });
    return;
  }
};

const filterSearchCriteria = (videos = [], searchCriteria = {}) => {
  let filteredVideos = videos;

  for (const [key, value] of Object.entries(searchCriteria)) {
    if (value !== "*" || (typeof value === Boolean && value !== false)) {
      filteredVideos = filteredVideos.filter((v) =>
        v[key].toLowerCase().includes(String(value).toLowerCase())
      );
    }
  }

  return filteredVideos;
};

// @desc    GET all error videos
// @route   /api/video/error/:databaseId
// @access  Public
export const getAllErrorListings = async (req, res, next) => {
  const { databaseId } = req.params;
  const db = await getDatabaseConn(databaseId);
  res.status(200).json({ errors: db.data.invalid });
};

// @desc    POST video to trigger synchronisation from FS by ID
// @route   /api/video/list/database/:databaseId/:id
// @access  Public
export const updateVideoFromFsById = async (req, res, next) => {
  const { databaseId, id } = req.params;
  const videoInfo = (await getStoredVideoById(databaseId, id)) || {};

  //TODO: perform video synchrnoisation,
};

// @desc    POST video to trigger fs synchronisation for entire database
// @route   /api/video/list/database/:databaseId
// @access  Public
export const updateAllListings = async (req, res, next) => {
  const { databaseId } = req.params;
  console.log(`updateAllListings`);
  const db = await getDatabaseConn(databaseId);
  const { videoFolderPath } = await getDatabaseSettingById(databaseId);
  // recursively find all files
  const paths = await create_video_map(videoFolderPath);
  db.data = {}; // refresh db

  // categorise valid and non-valid listings
  db.data.videos = Array.from(paths.values())
    .filter((p) => p.files.length > 0)
    .map((p, index) => {
      return { id: index + 1, ...p };
    });

  db.data.invalid = Array.from(paths.values()).filter(
    (p) => p.files.length === 0
  );

  db.write();

  res.status(200).json("done");
};
