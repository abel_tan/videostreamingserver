import { JSONPreset } from "lowdb/node";
import { DATABASE_TYPE_VIDEO } from "../constants/index.js";

// const DEFAULT_DATABASE = process.env.DATABASE;
const DEFAULT_DATABASE_DATA = {
  videos: [{ id: 0 }],
  likes: [{ uuid: "8f2db54f-35d1-5431-9022-5ce91077f9bc" }],
  invalidPaths: [{ id: 0 }],
};

export async function getDatabaseSettingById(databaseId = -1) {
  const db = await JSONPreset(
    process.env.SETTING_DATABASE,
    DEFAULT_DATABASE_DATA
  );
  return db.data.find(
    (setting) => String(setting.databaseId) === String(databaseId)
  );
}

// default configurations for settings database
export async function getSettingDatabaseConn() {
  return await JSONPreset(process.env.SETTING_DATABASE, DEFAULT_DATABASE_DATA);
}

export async function getDatabaseConn(
  databaseId,
  databaseType = DATABASE_TYPE_VIDEO
) {
  // retrieve video database from main settings
  const { databaseFilePath, databaseActionsFilePath } =
    await getDatabaseSettingById(databaseId);
  if (databaseType === DATABASE_TYPE_VIDEO) {
    return await JSONPreset(databaseFilePath, DEFAULT_DATABASE_DATA);
  } else {
    return await JSONPreset(databaseActionsFilePath, DEFAULT_DATABASE_DATA);
  }
}

export async function closeDatabaseConn(db) {
  await db.write();
}

export async function getSettingDatabaseLocation() {
  return process.env.SETTING_DATABASE;
}
