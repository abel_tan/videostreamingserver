import { Stack } from "@mui/material";
import TopPanel from "./components/global/TopPanel";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import MiddlePanel from "./components/global/MiddlePanel";
import VideoPlayerPopup from "./components/VideoPlayerPopup";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";
import SettingPopup from "./components/SettingPopup";
import Initializer from "./components/global/Initializer";

export default function MainPage() {
  const darkTheme = createTheme({
    palette: {
      mode: "dark",
    },
  });

  return (
    <ThemeProvider theme={darkTheme}>
      <CssBaseline />
      <Stack spacing={1}>
        {/* Initialize default values */}
        <Initializer />

        <TopPanel />
        <MiddlePanel />

        {/* Popup */}
        <VideoPlayerPopup />
        <SettingPopup />
        <ToastContainer position="bottom-right" theme="dark" autoClose={1500} />
      </Stack>
    </ThemeProvider>
  );
}
