import { toast } from "react-toastify";
import settingService from "../../services/settingService";
import { useDispatch, useSelector } from "react-redux";
import listService from "../../services/listService";
import { Button, Chip, Grid, Stack, TextField } from "@mui/material";
import {
  removeFromList,
  setList,
  setSelectedDatabaseId,
} from "../../features/setting/settingSlice";
import AnchorIcon from "@mui/icons-material/Anchor";
import SyncIcon from "@mui/icons-material/Sync";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";

// Form
export default function SettingForm({
  option = {},
  handleOnChange = () => {},
}) {
  const dispatch = useDispatch();
  const {
    title = "",
    databaseFilePath = "",
    databaseActionsFilePath = "",
    videoFolderPath = "",
    isSelected = false,
  } = option;
  const { selectedDatabaseId, list = [] } = useSelector(
    (state) => state.setting
  );

  const handleInput = (e) => {
    handleOnChange(
      (option = {
        ...option,
        [e.target.id]: e.target.value,
      })
    );

    // update database actions file path
    if (e.target.id === "databaseFilePath") {
      handleOnChange(
        (option = {
          ...option,
          databaseActionsFilePath: String(e.target.value).replace(
            ".json",
            ".actions.json"
          ),
        })
      );
    }
  };

  const getDatabaseActionsFilePath = () => {
    const { databaseFilePath } = option;
  };

  const handleSyncJob = () => {
    listService
      .updateVideoListings(selectedDatabaseId)
      .then((res) => {
        toast.success("FS Synchronised!");
      })
      .catch((err) => console.log(err));
  };

  function handleDeleteSettingById(databaseId) {
    settingService
      .deleteSettingById(databaseId)
      .then((res) => {
        dispatch(removeFromList(databaseId));
        toast.success("successfully delete from database");
      })
      .catch((err) => console.log(err));
  }

  function handleSelectDefaultOption(databaseId) {
    const updatedList = list.map((setting) => {
      if (String(databaseId) === String(setting.databaseId))
        return { ...setting, isSelected: true };
      else return { ...setting, isSelected: false };
    });

    settingService
      .updateSettings(updatedList)
      .then((res) => {
        dispatch(setList(updatedList));
        dispatch(
          setSelectedDatabaseId(
            updatedList.find((setting) => Boolean(setting.isSelected) === true)
              .databaseId
          )
        );
        toast.success("Database updated");
      })
      .catch((err) => console.log(err));
  }

  return (
    <Stack spacing={2} p={4} border={1} borderRadius={2}>
      <Stack
        direction="row"
        display="flex"
        alignItems="center"
        justifyContent="space-between"
      >
        <Stack direction="row" spacing={2}>
          <Chip
            label={Boolean(isSelected) ? `Default Selection` : `Not Selected`}
            color={Boolean(isSelected) ? `warning` : `error`}
          />
          <Button
            variant="outlined"
            onClick={() => handleSelectDefaultOption(option?.databaseId)}
            color="success"
            startIcon={<AnchorIcon />}
          >
            Select as default
          </Button>
        </Stack>
        <Stack direction="row" spacing={2}>
          <Button
            onClick={handleSyncJob}
            variant="outlined"
            color="warning"
            startIcon={<SyncIcon />}
          >
            Synchronise with FS
          </Button>

          <Button
            variant="outlined"
            onClick={() => handleDeleteSettingById(option?.databaseId)}
            color="error"
            startIcon={<DeleteForeverIcon />}
          >
            Delete Setting
          </Button>
        </Stack>
      </Stack>
      <Grid container direction="row" gap={2} alignItems="center">
        <Grid item xs={2}>
          <TextField
            id="title"
            label="Title"
            variant="outlined"
            value={title}
            fullWidth
            onChange={handleInput}
            autoComplete="off"
          />
        </Grid>
        <Grid item xs={4}>
          <TextField
            id="databaseFilePath"
            label="Database File Path"
            variant="outlined"
            value={`${databaseFilePath}`}
            fullWidth
            onChange={handleInput}
            autoComplete="off"
          />
          <TextField
            id="databaseActionsFilePath"
            label="Database Actions File Path"
            variant="outlined"
            value={`${databaseActionsFilePath}`}
            fullWidth
            onChange={handleInput}
            autoComplete="off"
            disabled
          />
        </Grid>
        <Grid item xs={3}>
          <TextField
            id="videoFolderPath"
            label="Video Directory"
            variant="outlined"
            value={videoFolderPath}
            fullWidth
            onChange={handleInput}
            autoComplete="off"
          />
        </Grid>
        <Grid item xs={2}></Grid>
      </Grid>
    </Stack>
  );
}
