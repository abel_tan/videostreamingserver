import { useEffect, useState } from "react";
import settingService from "../../services/settingService";
import { useSelector } from "react-redux";

export default function useSettingOptions() {
  const [settingOptions, setSettingOptions] = useState([]);
  const { list = [] } = useSelector((state) => state.setting);

  useEffect(() => {
    setSettingOptions(list);
  }, [list]);

  return [settingOptions, setSettingOptions];
}
