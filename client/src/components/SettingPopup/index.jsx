import AddBoxIcon from "@mui/icons-material/AddBox";
import SaveIcon from "@mui/icons-material/Save";
import { Box, Button, Stack } from "@mui/material";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { v4 as uuidv4 } from "uuid";
import {
  setIsSettingsOpened,
  setList,
} from "../../features/setting/settingSlice";
import settingService from "../../services/settingService";
import ColorToggleButton from "../ColorToggleButton";
import PopupDialog from "../PopupDialog";
import SettingForm from "./SettingForm";
import useSettingOptions from "./useSettingOptions";

export default function SettingPopup() {
  const dispatch = useDispatch();
  const { isSettingsOpened = false } = useSelector((state) => state.setting);
  const [settingOptions, setSettingOptions] = useSettingOptions();
  const [selectedOptionId, setSelectedOptionId] = useState(-1); // to manipulate children selection based on user selection

  const getSelectedOption = (options = []) => {
    // to inject user selection
    if (settingOptions !== -1)
      return settingOptions.find(
        (o) => String(o.databaseId) === String(selectedOptionId)
      );

    // default selection for on load purpose
    return options?.length > 0 && options[0];
  };

  const handleAddNewSetting = () => {
    const uuid = uuidv4();
    const defaultSetting = {
      databaseId: uuid,
      title: "NO_NAME",
      databaseFilePath: `C:\\database\\${uuid}.json`,
      videoFolderPath: "",
      isSelected: false,
    };
    setSettingOptions((prevState) => {
      return [...prevState, defaultSetting];
    });
    setSelectedOptionId(uuid);
  };

  // Submission
  function handleUpdateOptionSettings(settingOptions) {
    settingService
      .updateSettings(settingOptions)
      .then((res) => {
        dispatch(setList(settingOptions));
        toast.success("successfully updated database");
      })
      .catch((err) => console.log(err));
  }

  return (
    <PopupDialog
      isOpened={isSettingsOpened}
      handleOnClose={() => dispatch(setIsSettingsOpened(false))}
      title="Settings"
    >
      <Stack spacing={2}>
        <Stack display="flex" justifyContent="space-between" direction="row">
          <ColorToggleButton
            options={settingOptions}
            selectedOptionId={selectedOptionId}
            handleOnChange={setSelectedOptionId}
          />
          <Box>
            <Button
              startIcon={<AddBoxIcon />}
              onClick={() => handleAddNewSetting()}
              color="success"
              variant="outlined"
            >
              Add Settings
            </Button>

            <Button
              startIcon={<SaveIcon />}
              color="success"
              variant="outlined"
              onClick={() => handleUpdateOptionSettings(settingOptions)}
            >
              Save All
            </Button>
          </Box>
        </Stack>

        <SettingForm
          option={getSelectedOption(settingOptions)}
          handleOnChange={(v) =>
            setSettingOptions((prevState) => {
              return [
                ...prevState.filter(
                  (option) =>
                    String(option.databaseId) !== String(selectedOptionId)
                ),
                v,
              ];
            })
          }
        />
      </Stack>
    </PopupDialog>
  );
}
