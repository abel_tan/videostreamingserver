import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import videoService from "../../services/videoService";

export default function useSelectedVideoInfo() {
  const { selectedVideo } = useSelector((state) => state.selection);
  const { selectedDatabaseId } = useSelector((state) => state.setting);
  const [selectedVideoInfo, setSelectedVideoInfo] = useState({
    title: ``,
    files: [
      {
        subtitleApi: ``,
        videoApi: ``,
      },
    ],
  });

  useEffect(() => {
    if (Object.keys(selectedVideo).length > 0) {
      const { id, videoId } = selectedVideo;
      videoService
        .getVideoInfoById(selectedDatabaseId, id)
        .then((res) => {
          const { title, files = [] } = res;
          setSelectedVideoInfo({
            title,
            ...files.find((f) => f.videoId === videoId),
          });
        })
        .catch((err) => console.log(err));
    }
  }, [selectedVideo, selectedDatabaseId]);

  return [selectedVideoInfo, setSelectedVideoInfo];
}
