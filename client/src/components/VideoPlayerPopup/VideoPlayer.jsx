import { useState, useRef, useEffect } from "react";
import ReactPlayer from "react-player";
import screenfull from "screenfull";
import Container from "@mui/material/Container";
import ControlIcons from "./ControlIcons";
import { format } from "./formatter";
import useSelectedVideoInfo from "./useSelectedVideoInfo";
import { REACT_APP_BACKEND_URL } from "../../constants";

export default function VideoPlayer() {
  const playerRef = useRef(null);
  const playerDivRef = useRef(null);
  const [selectedVideoInfo] = useSelectedVideoInfo();

  const [playerstate, setPlayerState] = useState({
    playing: true,
    muted: false,
    volume: 0.5,
    playerbackRate: 1.0,
    played: 0,
    seeking: false,
  });

  let cachedPlayingState = true;

  //Destructure State in other to get the values in it
  const { playing, muted, volume, playerbackRate, played, seeking } =
    playerstate;
  const { title, videoApi, subtitleApi } = selectedVideoInfo;

  const currentPlayerTime = playerRef.current
    ? playerRef.current.getCurrentTime()
    : "00:00";
  const movieDuration = playerRef.current
    ? playerRef.current.getDuration()
    : "00:00";
  const playedTime = format(currentPlayerTime);
  const fullMovieTime = format(movieDuration);

  // handle user keyboard events
  useEffect(() => {
    function handleKeyDown(e) {
      if (e.keyCode === 39) handleFastForward();
      if (e.keyCode === 37) handleRewind();
      if (e.keyCode === 32) {
        cachedPlayingState = !cachedPlayingState;
        setPlayerState({ ...playerstate, playing: !cachedPlayingState });
      }

      // console.log(e.keyCode);
    }

    document.addEventListener("keydown", handleKeyDown);

    // Don't forget to clean up
    return function cleanup() {
      document.removeEventListener("keydown", handleKeyDown);
    };
  }, []);

  return (
    <>
      <Container maxWidth="xl">
        <div className="playerDiv" ref={playerDivRef}>
          <ReactPlayer
            onError={(e) => console.log(`player detected error`, e)}
            key={videoApi}
            width="100%"
            height="100%"
            ref={playerRef}
            url={`${REACT_APP_BACKEND_URL}${videoApi}`}
            // Force header range to allow streaming
            config={{
              file: {
                attributes: { crossOrigin: "anonymous" },
                tracks: [
                  {
                    kind: "subtitles",
                    src: `${REACT_APP_BACKEND_URL}${subtitleApi}`,
                    srcLang: "en",
                    default: true,
                  },
                ],
                hlsOptions: {
                  forceHLS: true,
                  xhrSetup: (xhr) => {
                    xhr.setRequestHeader(`range`, `bytes=0-`);
                  },
                },
              },
            }}
            playing={playing}
            volume={volume}
            playbackRate={playerbackRate}
            onProgress={handlePlayerProgress}
            muted={muted}
          />

          <ControlIcons
            key={volume.toString()}
            playandpause={handlePlayAndPause}
            playing={playing}
            rewind={handleRewind}
            fastForward={handleFastForward}
            muting={handleMuting}
            muted={muted}
            volumeChange={handleVolumeChange}
            volumeSeek={handleVolumeSeek}
            volume={volume}
            playerbackRate={playerbackRate}
            playRate={handlePlayerRate}
            fullScreenMode={handleFullScreenMode}
            played={played}
            onSeek={handlePlayerSeek}
            onSeekMouseUp={handlePlayerMouseSeekUp}
            onSeekMouseDown={handlePlayerMouseSeekDown}
            playedTime={playedTime}
            fullMovieTime={fullMovieTime}
            seeking={seeking}
            title={title}
          />
        </div>
      </Container>
    </>
  );

  //This function handles play and pause onchange button
  function handlePlayAndPause() {
    setPlayerState({ ...playerstate, playing: !playerstate.playing });
  }
  function handleMuting() {
    setPlayerState({ ...playerstate, muted: !playerstate.muted });
  }

  function handleRewind() {
    playerRef.current.seekTo(playerRef.current.getCurrentTime() - 10);
  }

  function handleFastForward() {
    playerRef.current.seekTo(playerRef.current.getCurrentTime() + 10);
  }

  function handleVolumeChange(e, newValue) {
    setPlayerState({
      ...playerstate,
      volume: parseFloat(newValue / 100),
      muted: newValue === 0 ? true : false,
    });
  }

  function handleVolumeSeek(e, newValue) {
    setPlayerState({
      ...playerstate,
      volume: parseFloat(newValue / 100),
      muted: newValue === 0 ? true : false,
    });
  }

  function handlePlayerRate(rate) {
    setPlayerState({ ...playerstate, playerbackRate: rate });
  }

  function handleFullScreenMode() {
    screenfull.toggle(playerDivRef.current);
  }

  function handlePlayerProgress(state) {
    // console.log("onProgress", state);
    if (!playerstate.seeking) {
      setPlayerState({ ...playerstate, ...state });
    }
  }
  function handlePlayerSeek(e, newValue) {
    setPlayerState({ ...playerstate, played: parseFloat(newValue / 100) });
    // console.log(`parseFloat(newValue / 100)`, parseFloat(newValue / 100));
    playerRef.current.seekTo(parseFloat(newValue / 100));
    // console.log(played)
  }

  function handlePlayerMouseSeekDown(e) {
    setPlayerState({ ...playerstate, seeking: true });
  }

  function handlePlayerMouseSeekUp(e, newValue) {
    setPlayerState({ ...playerstate, seeking: false });
    playerRef.current.seekTo(newValue / 100);
  }
}
