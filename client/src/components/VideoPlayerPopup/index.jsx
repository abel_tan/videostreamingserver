import React from "react";
import VideoPlayer from "./VideoPlayer";
import { useDispatch, useSelector } from "react-redux";
import PopupDialog from "../PopupDialog";
import {
  setIsPlayMode,
  setSelectedVideo,
} from "../../features/selection/selectionSlice";
import { Box, Chip, Stack } from "@mui/material";
import useVideoInfo from "./useVideoInfo";
import { v4 as uuidv4 } from "uuid";

export default function VideoPlayerPopup() {
  const dispatch = useDispatch();

  const { isPlayMode, selectedVideo } = useSelector((state) => state.selection);
  const [videoInfo] = useVideoInfo();

  return (
    <PopupDialog
      isOpened={isPlayMode}
      title={`${videoInfo.title} (id: ${selectedVideo.id}, videoId: ${selectedVideo.videoId})`}
      handleOnClose={() => dispatch(setIsPlayMode(false))}
    >
      <Stack direction="row" spacing={2}>
        <VideoPlayer />
        {videoInfo.files.length > 1 &&
          videoInfo.files.map((file, index) => {
            return (
              <Chip
                key={uuidv4()}
                label={`Part ${index + 1}`}
                clickable
                onClick={() => {
                  dispatch(
                    setSelectedVideo({
                      id: videoInfo.id,
                      videoId: file.videoId,
                    })
                  );
                }}
              />
            );
          })}
      </Stack>
    </PopupDialog>
  );
}
