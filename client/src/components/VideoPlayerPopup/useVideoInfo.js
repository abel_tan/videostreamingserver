import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import videoService from "../../services/videoService";

export default function useVideoInfo() {
  const { selectedVideo } = useSelector((state) => state.selection);
  const { selectedDatabaseId } = useSelector((state) => state.setting);
  const [videoInfo, setVideoInfo] = useState({
    title: ``,
    files: [
      {
        subtitleApi: ``,
        videoApi: ``,
      },
    ],
  });

  useEffect(() => {
    if (Object.keys(selectedVideo).length > 0) {
      const { id, videoId } = selectedVideo;
      videoService
        .getVideoInfoById(selectedDatabaseId, id)
        .then((res) => {
          const { title, files = [] } = res;
          setVideoInfo({
            id,
            title,
            files,
          });
        })
        .catch((err) => console.log(err));
    }
  }, [selectedVideo, selectedDatabaseId]);

  return [videoInfo, setVideoInfo];
}
