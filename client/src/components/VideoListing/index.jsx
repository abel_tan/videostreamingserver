import { Button, Grid, Stack, Typography } from "@mui/material";
import React, { useState } from "react";
import { v4 as uuid } from "uuid";
import useVideoListings from "./useVideoListings";
import VideoBox from "../VideoBox";
import { useDispatch } from "react-redux";
import { setOrderedByAsc } from "../../features/setting/settingSlice";

export default function VideoListing() {
  const dispatch = useDispatch();

  const [videoListings] = useVideoListings();
  const [ordering, setOrdering] = useState(true); // sort asc by default

  if (videoListings.length === 0)
    return <Typography>No videos found</Typography>;

  return (
    <Stack rowGap={1}>
      <Stack
        direction="row"
        display="flex"
        justifyContent="space-between"
        alignItems="center"
      >
        <Typography>{`Total Listings: ${videoListings.length}`}</Typography>
        <Stack direction="row" spacing={2} alignItems="center">
          <Button
            onClick={() => {
              dispatch(setOrderedByAsc(!ordering));
              setOrdering((prevState) => !prevState);
            }}
            variant="outlined"
          >
            {ordering ? "ASC" : "DESC"}
          </Button>
        </Stack>
      </Stack>
      <Grid container spacing={4}>
        {videoListings?.map((vl) => {
          return (
            <Grid item xs={2} key={uuid()}>
              <VideoBox id={vl.id} />
            </Grid>
          );
        })}
      </Grid>
    </Stack>
  );
}
