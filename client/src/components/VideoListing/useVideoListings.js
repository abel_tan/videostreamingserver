import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { REACT_APP_DEFAULT_VIDEO_PER_PAGE } from "../../constants";
import { doSortById } from "../../utils";

export default function useVideoListings() {
  const [videoListings, setVideoListings] = useState([]);
  const { videosPerPage, orderedByAsc } = useSelector((state) => state.setting);
  const { originalVideos = [] } = useSelector((state) => state.databaseVideo);

  const { criteria } = useSelector((state) => state.search);

  // listener to update video listing displayed
  useEffect(() => {
    if (
      originalVideos.length > 0 &&
      videosPerPage !== undefined &&
      orderedByAsc !== undefined &&
      Object.keys(criteria).length > 0
    ) {
      // sort list
      let filteredVideos = doSortById(
        originalVideos,
        "oldestBirthDate",
        orderedByAsc
      );

      // filter list
      for (const [key, value] of Object.entries(criteria)) {
        // filter video listings to page
        filteredVideos = filteredVideos.filter((v) => {
          if (isNoFilter(value)) return true;
          else if (isArraySubsetOf(v[key], value)) return true;
          else if (isSubsetOf(v[key], value)) return true;
          else if (isBooleanEquals(v[key], value)) return true;
          else return false;
        });
      }

      // limit list
      filteredVideos = doLimit(filteredVideos, videosPerPage);

      setVideoListings(filteredVideos);
    }
  }, [originalVideos, videosPerPage, orderedByAsc, criteria]);

  return [videoListings, setVideoListings];
}

// filtering criterias
function isNoFilter(value = "") {
  return String(value) === "*";
}

function isArraySubsetOf(valueA, valueB) {
  if (Array.isArray(valueA)) {
    return valueA.includes(valueB);
  } else {
    return false;
  }
}

function isSubsetOf(valueA, valueB) {
  if (
    typeof valueA === "string" &&
    valueA.toLowerCase().includes(valueB.toLowerCase())
  )
    return true;
  else return false;
}

// this is a loose function to allow like selection to go thru when it is undefined, as this is not populated during backend processing when there is no likes
function isBooleanEquals(valueA, valueB) {
  // if user selection is not a boolean (default to valueB parameter)
  if (!isBoolean(valueB)) {
    return false;
  } else if (String(valueB) === "false" && valueA === undefined) {
    // if user selection for like is false
    return true;
  } else if (String(valueB) === "true" && String(valueA) === String(valueB))
    return true;
  else return false;
}

function isBoolean(value) {
  return Boolean(value) === false || Boolean(value) === true;
}

function doLimit(videos = [], limit = REACT_APP_DEFAULT_VIDEO_PER_PAGE) {
  // return default videos;
  if (videos.length === 0 || Number(limit) <= 0) return videos;

  // limit list
  return videos.splice(0, limit);
}
