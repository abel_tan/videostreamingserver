import * as React from "react";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";

export default function DropdownList({
  label = "",
  options = [],
  handleOnChange = () => {},
  defaultValue = "",
}) {
  const [userSelection, setUserSelection] = React.useState(defaultValue);

  const handleChange = (e) => {
    setUserSelection(e.target.value);
    handleOnChange(e.target.value);
  };

  return (
    <Box sx={{ minWidth: 120 }} width="100%">
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">{label}</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={userSelection}
          label={label}
          onChange={handleChange}
        >
          {options.map((o) => {
            return (
              <MenuItem value={o} key={o}>
                {o}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
    </Box>
  );
}
