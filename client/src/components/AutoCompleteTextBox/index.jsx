import * as React from "react";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import { InputAdornment } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";

export default function AutoCompleteTextBox({
  label = "Search",
  options = [
    { label: "test1", id: 1, key: 1 },
    { label: "test2", id: 2, key: 2 },
  ],
  handleOnChange = () => {},
}) {
  const [userInput, setUserInput] = React.useState("");
  const handleChange = (e) => {
    if (typeof e.target.value === "string") {
      handleOnChange(e.target.value);
      setUserInput(e.target.value);
    } else {
      handleOnChange(options[e.target.value].label);
      setUserInput(options[e.target.value]);
    }
  };

  return (
    <Autocomplete
      disablePortal
      id="combo-box-demo"
      options={options}
      value={userInput}
      fullWidth
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          InputProps={{
            ...params.InputProps,
            startAdornment: (
              <InputAdornment position="start">
                <SearchIcon />
              </InputAdornment>
            ),
          }}
        />
      )}
      isOptionEqualToValue={(selectedOption, value) =>
        String(selectedOption.label) === String(value.label)
      }
      freeSolo
      getOptionLabel={(option) => option?.label || userInput}
      onChange={handleChange}
    />
  );
}
