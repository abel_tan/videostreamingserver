import {
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";

export default function PopupDialog({
  isOpened = false,
  handleOnClose = () => {},
  children,
  title = "Please provide a title",
}) {
  return (
    <Dialog
      fullWidth={true}
      maxWidth="xl"
      open={isOpened}
      onClose={handleOnClose}
    >
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>
        {/* <DialogContentText>
          You can set my maximum width and whether to adapt or not.
        </DialogContentText> */}
        {children}
      </DialogContent>
      {/* <DialogActions></DialogActions> */}
    </Dialog>
  );
}
