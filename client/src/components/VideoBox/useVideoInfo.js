import { useEffect, useState } from "react";
import videoService from "../../services/videoService";
import { useSelector } from "react-redux";

export default function useVideoInfo(id) {
  const [videoInfo, setVideoInfo] = useState({});
  const { selectedDatabaseId } = useSelector((state) => state.setting);

  useEffect(() => {
    if (id !== -1 && selectedDatabaseId !== "") {
      videoService
        .getVideoInfoById(selectedDatabaseId, id)
        .then((res) => {
          setVideoInfo(res);
        })
        .catch((err) => console.log(err));
    }
  }, [id, selectedDatabaseId]);

  return [videoInfo, setVideoInfo];
}
