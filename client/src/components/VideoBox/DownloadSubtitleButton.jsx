import CloudDownloadIcon from "@mui/icons-material/CloudDownload";
import { IconButton, Tooltip } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import videoService from "../../services/videoService";
import { triggerRefreshSite } from "../../features/setting/settingSlice";
import SevereColdIcon from "@mui/icons-material/SevereCold";

export default function DownloadSubtitleButton({
  id,
  hasSubtitles = false,
  hasDownloadIssue = false,
}) {
  const { selectedDatabaseId } = useSelector((state) => state.setting);
  const dispatch = useDispatch();

  const handleDownload = async () => {
    videoService
      .updateSubtitleById(selectedDatabaseId, id)
      .then((res) => {
        toast.success("Successfully downloaded and updated database");
        dispatch(triggerRefreshSite());
      })
      .catch((err) => {
        const { data } = err.response;
        toast.error(data);
        dispatch(triggerRefreshSite());
      });
  };
  return (
    <Tooltip
      title={
        hasDownloadIssue ? "Download failed previously" : "Download Subtitles"
      }
    >
      <IconButton onClick={handleDownload}>
        {hasDownloadIssue ? (
          <SevereColdIcon color="error" />
        ) : (
          <CloudDownloadIcon color={hasSubtitles ? "disabled" : "success"} />
        )}
      </IconButton>
    </Tooltip>
  );
}
