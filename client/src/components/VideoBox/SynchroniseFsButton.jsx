import SyncIcon from "@mui/icons-material/Sync";
import { IconButton, Tooltip } from "@mui/material";

export default function SynchroniseFsButton({ id }) {
  return (
    <Tooltip title="Synchronise FS video">
      <IconButton>
        <SyncIcon />
      </IconButton>
    </Tooltip>
  );
}
