import FavoriteIcon from "@mui/icons-material/Favorite";
import ModeEditIcon from "@mui/icons-material/ModeEdit";
import MoreIcon from "@mui/icons-material/More";
import {
  Box,
  Card,
  CardActions,
  CardHeader,
  CardMedia,
  Chip,
  IconButton,
  Stack,
  Tooltip,
  Typography,
} from "@mui/material";
import dayjs from "dayjs";
import { useDispatch, useSelector } from "react-redux";
import { REACT_APP_BACKEND_URL } from "../../constants";
import {
  setIsPlayMode,
  setSelectedVideo,
} from "../../features/selection/selectionSlice";
import likeService from "../../services/likeService";
import DownloadSubtitleButton from "./DownloadSubtitleButton";
import SynchroniseFsButton from "./SynchroniseFsButton";
import useVideoInfo from "./useVideoInfo";
import { v4 as uuid } from "uuid";
import DownloadTagButton from "./DownloadTagButton";

export default function VideoBox({ id }) {
  const dispatch = useDispatch();
  const { selectedDatabaseId } = useSelector((state) => state.setting);

  const [videoInfo, setVideoInfo] = useVideoInfo(id);
  const {
    title = "no title",
    thumbnailApi = "",
    label = "",
    uuid = "",
    oldestBirthDate,
    like = false,
    files = [],
    subtitleExists = false,
    isSubtitleDownloadFailed = false,
    isTagDownloadFailed = false,
    tags = [],
  } = videoInfo;
  return (
    <Card
      sx={{
        height: 480,
        width: 380,
        ":hover": {
          // boxShadow: 4,  // not working for dark mode
          cursor: "pointer",
          opacity: 0.9,
        },
      }}
    >
      <CardHeader
        sx={{ font: "white" }}
        title={
          <Tooltip title={title}>
            <Typography color="white">{formatTitle(title)}</Typography>
          </Tooltip>
        }
        action={[
          <IconButton
            aria-label="edit"
            onClick={() => console.log("moreeditbutton")}
            key="edit_button"
          >
            <ModeEditIcon />
          </IconButton>,
          <IconButton
            aria-label="show-more"
            onClick={() => console.log(`moreicon button`)}
            key="show_more_button"
          >
            <Tooltip title={JSON.stringify(videoInfo)}>
              <MoreIcon />
            </Tooltip>
          </IconButton>,
        ]}
        subheader={dayjs(oldestBirthDate).format("DD-MMM-YY")}
      />
      <CardMedia
        component="img"
        height={280}
        image={`${REACT_APP_BACKEND_URL}${thumbnailApi}`}
        onClick={() => {
          dispatch(setSelectedVideo({ id, videoId: 1 })); // plays first video in metadata
          dispatch(setIsPlayMode(true));
        }}
      />
      <CardActions disableSpacing>
        <Stack width="100%">
          <Stack
            direction="row"
            spacing={1}
            alignItems="center"
            justifyContent="space-between"
          >
            <Box>
              <Tooltip title={like ? `Unlike` : `Like`}>
                <IconButton
                  aria-label="add to favorites"
                  onClick={() => {
                    likeService.patchLikeById(selectedDatabaseId, uuid, {
                      uuid,
                      like: !like,
                    });
                    setVideoInfo({ ...videoInfo, like: !like });
                  }}
                >
                  <FavoriteIcon sx={{ color: Boolean(like) ? "red" : "" }} />
                </IconButton>
              </Tooltip>
              <ShowVideoParts files={files} />
              <ShowSubtitleExists subtitleExists={subtitleExists} />
            </Box>

            <Stack direction="row" alignItems="center">
              {/* <Chip size="small" label={label} /> */}
              {/* <SynchroniseFsButton id={id} /> */}
              <DownloadSubtitleButton
                id={id}
                hasSubtitles={subtitleExists}
                hasDownloadIssue={isSubtitleDownloadFailed}
              />
              <DownloadTagButton
                id={id}
                hasTags={tags?.length > 0}
                hasDownloadIssue={isTagDownloadFailed}
              />
            </Stack>
          </Stack>
          <Stack direction="row" pl={1} spacing={4}>
            <ShowTags tags={videoInfo?.tags} />

            {/* <ShowDateAdded videoInfo={videoInfo} /> */}
            {/* <ShowTotalFiles videoInfo={videoInfo} /> */}
            {/* <ShowTotalFileSize videoInfo={videoInfo} /> */}
          </Stack>
        </Stack>
      </CardActions>
    </Card>
  );
}

function ShowTags({ tags = [] }) {
  if (tags.length === 0) return <></>;
  return (
    <Stack
      direction="row"
      alignItems="center"
      width="100%"
      flexWrap="wrap"
      gap={1}
    >
      <Typography color="lightgray" fontSize={14}>
        Tags
      </Typography>

      {tags?.map((tag) => (
        <Chip label={tag} size="small" key={uuid()} />
      ))}
    </Stack>
  );
}

function ShowTotalFiles({ videoInfo }) {
  if (!videoInfo) return <></>;
  if (!videoInfo?.files) return <></>;

  return (
    <Stack>
      <Typography color="lightgray" fontSize={14}>
        Total Files
      </Typography>
      <Typography fontSize={14}>{videoInfo?.files.length}</Typography>
    </Stack>
  );
}

function ShowDateAdded({ videoInfo }) {
  if (!videoInfo) return <></>;
  if (!videoInfo?.files) return <></>;
  if (!videoInfo?.files.length === 0) return <></>;

  return (
    <Stack>
      <Typography color="lightgray" fontSize={14}>
        Date Added
      </Typography>
      <Typography fontSize={14}>
        {dayjs(videoInfo.files[0].birthtime).format("DD/MM/YYYY")}
      </Typography>
    </Stack>
  );
}

function ShowTotalFileSize({ videoInfo }) {
  if (!videoInfo) return <></>;
  if (!videoInfo?.files) return <></>;
  if (!videoInfo?.files.length === 0) return <></>;

  const readableBytes = (bytes) => {
    const i = Math.floor(Math.log(bytes) / Math.log(1024)),
      sizes = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

    return (bytes / Math.pow(1024, 2)).toFixed(2) * 1 + " " + sizes[2];
  };

  return (
    <Stack>
      <Typography color="lightgray" fontSize={14}>
        Total Size
      </Typography>
      <Typography fontSize={14}>
        {`${readableBytes(
          videoInfo.files.reduce((totalSize, f) => {
            return Number(totalSize) + Number(f.size);
          }, 0)
        )}`}
      </Typography>
    </Stack>
  );
}

function ShowVideoParts({ files = [] }) {
  return (
    files.length > 1 && <Chip size="small" label={`Parts: ${files.length}`} />
  );
}

function ShowSubtitleExists({ subtitleExists = false }) {
  return subtitleExists && <Chip size="small" label={`Subtitled`} />;
}

function formatTitle(title = "") {
  if (title.length > 25) {
    return String(title).slice(0, 25).concat("...");
  } else {
    return title;
  }
}
