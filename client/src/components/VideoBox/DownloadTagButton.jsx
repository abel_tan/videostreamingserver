import { IconButton, Tooltip } from "@mui/material";
import videoService from "../../services/videoService";
import DescriptionIcon from "@mui/icons-material/Description";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { triggerRefreshSite } from "../../features/setting/settingSlice";
import LocalOfferIcon from "@mui/icons-material/LocalOffer";
import LabelOffIcon from "@mui/icons-material/LabelOff";

export default function DownloadTagButton({
  id,
  hasTags = false,
  hasDownloadIssue = false,
}) {
  const { selectedDatabaseId } = useSelector((state) => state.setting);
  const dispatch = useDispatch();

  const handleUpdateTagById = (id) => {
    videoService
      .updateTagById(selectedDatabaseId, id)
      .then((res) => {
        toast.success("Successfully downloaded and updated database");
        dispatch(triggerRefreshSite());
      })
      .catch((err) => {
        toast.error("Failed to update tags");
        dispatch(triggerRefreshSite());
      });
  };

  return (
    <Tooltip
      title={hasDownloadIssue ? "Download failed previously" : "Download Tags"}
    >
      <IconButton onClick={() => handleUpdateTagById(id)}>
        {hasDownloadIssue ? (
          <LabelOffIcon color="error" />
        ) : (
          <LocalOfferIcon color={hasTags ? "disabled" : "success"} />
        )}
      </IconButton>
    </Tooltip>
  );
}
