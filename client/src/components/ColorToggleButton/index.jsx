import ToggleButton from "@mui/material/ToggleButton";
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup";
import { useEffect, useState } from "react";
import AnchorIcon from "@mui/icons-material/Anchor";

const DEFAULT_OPTIONS = [
  { databaseId: 1, title: "Web" },
  { databaseId: 2, title: "Android" },
];
export default function ColorToggleButton({
  options = DEFAULT_OPTIONS,
  selectedOptionId = -1,
  handleOnChange = () => {},
}) {
  const [alignment, setAlignment] = useState(DEFAULT_OPTIONS[0].databaseId);

  useEffect(() => {
    if (selectedOptionId !== -1) {
      setAlignment(selectedOptionId);
    }
  }, [selectedOptionId]);

  const handleChange = (event, newAlignment) => {
    if (newAlignment === null) return;
    handleOnChange(
      options.find((o) => String(o.databaseId) === String(newAlignment))
        .databaseId
    );
    setAlignment(newAlignment);
  };

  return (
    <ToggleButtonGroup
      color="primary"
      value={alignment}
      exclusive
      onChange={handleChange}
      aria-label="Platform"
    >
      {options?.map((o) => {
        return (
          <ToggleButton value={o.databaseId} key={o.databaseId}>
            {o.title}
            {o?.isSelected && <AnchorIcon />}
          </ToggleButton>
        );
      })}
    </ToggleButtonGroup>
  );
}
