import { Stack } from "@mui/material";
import VideoListing from "../../VideoListing";

export default function MiddlePanel() {
  return (
    <Stack p={3}>
      <VideoListing />
    </Stack>
  );
}
