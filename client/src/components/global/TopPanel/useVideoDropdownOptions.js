import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import listService from "../../../services/listService";
import { doSortById } from "../../../utils";

export default function useVideoDropdownOptions() {
  const [titleOptions, setTitleOptions] = useState([]);
  const [labelOptions, setLabelOptions] = useState([]);
  const [tagOptions, setTagOptions] = useState([]);
  const { selectedDatabaseId } = useSelector((state) => state.setting);

  useEffect(() => {
    if (selectedDatabaseId !== "") {
      listService
        .getVideoListings(selectedDatabaseId)
        .then((res) => {
          const { videos } = res;

          // format to align with ddl options requirements
          setTitleOptions(
            doSortById(
              videos.map((v) => {
                return {
                  ...v,
                  label: v.title,
                };
              }),
              "title"
            )
          );
          // format to align with ddl options requirements
          setLabelOptions([...new Set(videos.map((v) => v.label))].sort());
          // format to align with ddl options requirements
          setTagOptions(
            [
              ...new Set(
                videos
                  .filter((v) => v.tags !== undefined)
                  .map((v) => v.tags)
                  .flat()
              ),
            ].sort()
          );
        })
        .catch((err) => console.log(err));
    }
  }, [selectedDatabaseId]);

  return [
    titleOptions,
    setTitleOptions,
    labelOptions,
    setLabelOptions,
    tagOptions,
    setTagOptions,
  ];
}
