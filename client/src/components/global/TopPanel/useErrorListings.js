import { useEffect, useState } from "react";
import listService from "../../../services/listService";
import { useSelector } from "react-redux";

export default function useErrorListings() {
  const [errorListings, setErrorListings] = useState([]);
  const { selectedDatabaseId } = useSelector((state) => state.setting);

  useEffect(() => {
    if (selectedDatabaseId !== "") {
      listService
        .getAllErrorListings(selectedDatabaseId)
        .then((res) => {
          const { errors = [] } = res;
          setErrorListings(errors);
        })
        .catch((err) => console.log(err));
    }
  }, [selectedDatabaseId]);
  return [errorListings, setErrorListings];
}
