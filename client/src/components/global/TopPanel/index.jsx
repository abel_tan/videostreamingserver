import { AppBar, Chip, Container, Stack, Toolbar } from "@mui/material";
import * as React from "react";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import SmartDisplayIcon from "@mui/icons-material/SmartDisplay";
import SettingsIcon from "@mui/icons-material/Settings";
import useErrorListings from "./useErrorListings";
import RotateLeftIcon from "@mui/icons-material/RotateLeft";
import ErrorIcon from "@mui/icons-material/Error";
import {
  setIsSettingsOpened,
  setVideosPerPage,
  triggerRefreshSite,
} from "../../../features/setting/settingSlice";
import { useDispatch, useSelector } from "react-redux";
import DropdownList from "../../DropdownList";
import { reset, updateCriteria } from "../../../features/search/searchSlice";
import AutoCompleteTextBox from "../../AutoCompleteTextBox";
import { REACT_APP_DEFAULT_VIDEO_PER_PAGE } from "../../../constants";
import useVideoDropdownOptions from "./useVideoDropdownOptions";

const SHOW_ALL_OPTION = "*";

export default function TopPanel() {
  const dispatch = useDispatch();

  const [errorListings] = useErrorListings();
  const [
    titleOptions,
    setTitleOptions,
    labelOptions,
    setLabelOptions,
    tagOptions,
    setTagOptions,
  ] = useVideoDropdownOptions();
  const { selectedDatabaseId = "", list = [] } = useSelector(
    (state) => state.setting
  );
  const { criteria = {} } = useSelector((state) => state.search);

  return (
    <AppBar position="sticky">
      <Container maxWidth="100%">
        <Toolbar
          disableGutters
          sx={{
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <Logo />
          <Stack direction="row" spacing={2} width={1000} p={2}>
            <AutoCompleteTextBox
              label="Search"
              options={titleOptions}
              handleOnChange={(value) =>
                dispatch(updateCriteria({ title: value }))
              }
            />
            <DropdownList
              label="Search By Label"
              options={[SHOW_ALL_OPTION, ...labelOptions]}
              handleOnChange={(value) =>
                dispatch(updateCriteria({ label: value }))
              }
            />
            <DropdownList
              label="Filter Likes"
              options={[SHOW_ALL_OPTION, "true", "false"]}
              handleOnChange={(value) =>
                dispatch(updateCriteria({ like: value }))
              }
            />
            <DropdownList
              label="Filter Tags"
              options={[SHOW_ALL_OPTION, ...tagOptions]}
              handleOnChange={(value) =>
                dispatch(updateCriteria({ tags: value }))
              }
            />
            <Button onClick={() => dispatch(reset())}>Reset</Button>
          </Stack>
          <Stack direction="row" spacing={2} alignItems="center" width={500}>
            <Stack direction="row" width={300} alignItems="center">
              <ManageReloadButton />
              <ManageErrorButton title={`${errorListings.length}`} />
            </Stack>
            <Chip
              label={`${
                list?.find(
                  (setting) =>
                    String(setting.databaseId) === String(selectedDatabaseId)
                )?.title || "err"
              }`}
              color="success"
            />

            <DropdownList
              label="Videos per page"
              options={[10, 50, 100, 1000, 10000]}
              handleOnChange={(v) => dispatch(setVideosPerPage(v))}
              defaultValue={REACT_APP_DEFAULT_VIDEO_PER_PAGE}
            />
            <Tooltip title="Settings">
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={() => dispatch(setIsSettingsOpened(true))}
                color="inherit"
              >
                <SettingsIcon />
              </IconButton>
            </Tooltip>
          </Stack>
        </Toolbar>
      </Container>
    </AppBar>
  );
}

function ManageErrorButton({ title = "" }) {
  return (
    <Tooltip title={`Total of ${title} errors found`}>
      <Button color="error" startIcon={<ErrorIcon />} size="medium">
        Errors
      </Button>
    </Tooltip>
  );
}

function ManageReloadButton({}) {
  const dispatch = useDispatch();
  return (
    <Tooltip title="Reload Site">
      <Button
        color="success"
        onClick={() => dispatch(triggerRefreshSite())}
        startIcon={<RotateLeftIcon />}
        size="medium"
      >{`Reload`}</Button>
    </Tooltip>
  );
}

function Logo() {
  return (
    <Stack direction="row" alignItems="center" spacing={2}>
      <SmartDisplayIcon sx={{ display: { xs: "none", md: "flex" } }} />
      <Typography
        variant="h6"
        noWrap
        component="a"
        href="#app-bar-with-responsive-menu"
        color="orange"
        sx={{
          display: { xs: "none", md: "flex" },
          fontFamily: "monospace",
          fontWeight: 700,
          letterSpacing: ".2rem",
          textDecoration: "none",
        }}
      >
        MOVIE THEATRE
      </Typography>
    </Stack>
  );
}
