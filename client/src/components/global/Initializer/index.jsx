import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  setList,
  setSelectedDatabaseId,
} from "../../../features/setting/settingSlice";
import settingService from "../../../services/settingService";
import listService from "../../../services/listService";
import { setOriginalVideos } from "../../../features/databaseVideo/databaseVideoSlice";
import { toast } from "react-toastify";
import { REACT_APP_MAX_VIDEO_PER_PAGE } from "../../../constants";

export default function Initializer() {
  const dispatch = useDispatch();
  const { selectedDatabaseId } = useSelector((state) => state.setting);

  // retrieve default values - onload
  useEffect(() => {
    settingService
      .getAllSettings()
      .then((res) => {
        dispatch(setList(res)); // update redux with full list of settings

        const index = res.findIndex(
          (setting) => Boolean(setting.isSelected) === true
        );

        if (index === -1) {
          dispatch(setSelectedDatabaseId(res[0].databaseId));
        } else {
          dispatch(setSelectedDatabaseId(res[index].databaseId));
        }
      })
      .catch((err) => console.log(err));
  }, []);

  // retrieve default values - after settings loaded, to load video listings
  useEffect(() => {
    if (selectedDatabaseId) {
      listService
        .getVideoListings(selectedDatabaseId, REACT_APP_MAX_VIDEO_PER_PAGE)
        .then((res) => dispatch(setOriginalVideos(res.videos)))
        .catch((err) => toast.error(err));
    }
  }, [selectedDatabaseId]);

  return <></>;
}
