export const ENV_OPTION_DEVELOPMENT = "development";
export const IS_DEV =
  process.env.NODE_ENV === ENV_OPTION_DEVELOPMENT ? true : false;

export const REACT_APP_DEV_BACKEND_URL = "http://localhost:3000";
export const REACT_APP_BACKEND_URL = IS_DEV ? "http://localhost:3000" : "";
export const API_URL_THUMBNAIL = "/api/video/thumbnail";

export const REACT_APP_DEFAULT_VIDEO_PER_PAGE = 50;
export const REACT_APP_MAX_VIDEO_PER_PAGE = 10000;
// export const API_URL_SUBTITLE = "http://localhost:3000/api/video/subtitle";
// export const API_URL_VIDEO = "http://localhost:3000/api/video/file";
