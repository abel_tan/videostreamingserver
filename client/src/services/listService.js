import { axiosClient } from "../configs/apiClient";
import { REACT_APP_DEFAULT_VIDEO_PER_PAGE } from "../constants";

const API_URL_VIDEO_LISTING = "/api/video/list";
const API_URL_ERROR_LISTING = "/api/video/error";

const getVideoListings = async (
  databaseId,
  videosPerPage = REACT_APP_DEFAULT_VIDEO_PER_PAGE,
  orderedByAsc = true,
  searchCriteria = {}
) => {
  const searchParams = new URLSearchParams({
    videosPerPage,
    orderedByAsc,
  });

  searchParams.append(
    "searchCriteria",
    encodeURIComponent(JSON.stringify(searchCriteria))
  );

  const response = await axiosClient.get(
    `${API_URL_VIDEO_LISTING}/${databaseId}?${searchParams.toString()}`
  );

  return response.data;
};

// performs update on host OS to retrieve and update database with latesting video listings
const updateVideoListings = async (databaseId) => {
  const response = await axiosClient.post(
    `${API_URL_VIDEO_LISTING}/database/${databaseId}`,
    {
      triggerUpdate: true,
    }
  );
  return response.data;
};

const getAllErrorListings = async (databaseId) => {
  const response = await axiosClient.get(
    `${API_URL_ERROR_LISTING}/${databaseId}`
  );
  return response.data;
};

// allows autocomplete in search feature
const getVideoTitlesBySearch = async (databaseId, searchCriteria) => {
  const videosPerPage = 10; // default search filter results
  const searchParams = new URLSearchParams(videosPerPage);

  searchParams.append(
    "searchCriteria",
    encodeURIComponent(JSON.stringify(searchCriteria))
  );
  const response = await axiosClient.get(
    `${API_URL_VIDEO_LISTING}/${databaseId}?${searchParams.toString()}`
  );

  return response.data;
};

const listService = {
  getVideoListings,
  updateVideoListings,
  getAllErrorListings,
  getVideoTitlesBySearch,
};

export default listService;
