import { axiosClient } from "../configs/apiClient";
const API_URL_LIKE = "/api/video/like";

const patchLikeById = async (databaseId, uuid, data) => {
  const response = await axiosClient.patch(
    `${API_URL_LIKE}/${databaseId}/${uuid}`,
    data
  );
  return response.data;
};

const likeService = {
  patchLikeById,
};

export default likeService;
