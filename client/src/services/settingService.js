import { axiosClient } from "../configs/apiClient";

const API_URL_SETTING = "/api/setting";

const getAllSettings = async () => {
  const response = await axiosClient.get(`${API_URL_SETTING}`);
  return response.data;
};

// sends updated settings
const updateSettings = async (data) => {
  const response = await axiosClient.post(`${API_URL_SETTING}`, data);
  return response.data;
};

// sends updated setting by id
const deleteSettingById = async (id) => {
  const response = await axiosClient.delete(`${API_URL_SETTING}/${id}`);
  return response.data;
};

const settingService = {
  getAllSettings,
  updateSettings,
  deleteSettingById,
};

export default settingService;
