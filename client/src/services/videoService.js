import { axiosClient } from "../configs/apiClient";

const API_URL_VIDEOS = "/api/video/file";
const API_URL_SUBTITLE = "/api/video/subtitle";
const API_URL_TAG = "/api/video/tag";
const API_URL_THUMBNAIL = "/api/video/thumbnail";
const API_URL_INFO = "/api/video/info";

const getThumbnailById = async (databaseId, id) => {
  const response = await axiosClient.get(
    `${API_URL_THUMBNAIL}/${databaseId}/${id}`
  );
  return response.data;
};

const getVideoById = async (databaseId, id) => {
  const response = await axiosClient.get(
    `${API_URL_VIDEOS}/${databaseId}/${id}`
  );
  return response.data;
};

const getVideoInfoById = async (databaseId, id) => {
  const response = await axiosClient.get(`${API_URL_INFO}/${databaseId}/${id}`);
  return response.data;
};

const getSubtitleById = async (databaseId, id) => {
  const response = await axiosClient.get(
    `${API_URL_SUBTITLE}/${databaseId}/${id}`
  );
  return response.data;
};

const updateSubtitleById = async (databaseId, id) => {
  const response = await axiosClient.post(
    `${API_URL_SUBTITLE}/${databaseId}/${id}`
  );
  return response.data;
};

const updateTagById = async (databaseId, id) => {
  const response = await axiosClient.post(`${API_URL_TAG}/${databaseId}/${id}`);
  return response.data;
};

const videoService = {
  getThumbnailById,
  getVideoById,
  getVideoInfoById,
  getSubtitleById,
  updateSubtitleById,
  updateTagById,
};

export default videoService;
