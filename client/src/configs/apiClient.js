import axios from "axios";
import { IS_DEV, REACT_APP_DEV_BACKEND_URL } from "../constants";

const getNewAxiosClient = () => {
  return IS_DEV
    ? axios.create({
        baseURL: `${REACT_APP_DEV_BACKEND_URL}`,
      })
    : axios.create();
};

export const axiosClient = getNewAxiosClient();
axiosClient.interceptors.request.use(
  (config) => {
    config.params = config.params || {};
    config.headers[`Accept`] = "application/json";
    config.headers[`Content-Type`] = "application/json";
    config.headers[`timeout`] = 1000;
    return config;
  },
  (error) => Promise.reject(error)
);
