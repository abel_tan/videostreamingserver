import { configureStore } from "@reduxjs/toolkit";
import selectionReducer from "../features/selection/selectionSlice";
import settingReducer from "../features/setting/settingSlice";
import searchReducer from "../features/search/searchSlice";
import databaseVideoReducer from "../features/databaseVideo/databaseVideoSlice";

export default configureStore({
  reducer: {
    selection: selectionReducer,
    setting: settingReducer,
    search: searchReducer,
    databaseVideo: databaseVideoReducer,
  },
});
