import { createSlice } from "@reduxjs/toolkit";

export const selectionSlice = createSlice({
  name: "selection",
  initialState: {
    selectedVideo: {},
    isPlayMode: false,
  },
  reducers: {
    setSelectedVideo: (state, action) => {
      state.selectedVideo = action.payload;
    },
    setIsPlayMode: (state, action) => {
      state.isPlayMode = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setSelectedVideo, setIsPlayMode } = selectionSlice.actions;

export default selectionSlice.reducer;
