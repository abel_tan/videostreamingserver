import { createSlice } from "@reduxjs/toolkit";
import { REACT_APP_DEFAULT_VIDEO_PER_PAGE } from "../../constants";

export const settingSlice = createSlice({
  name: "setting",
  initialState: {
    selectedDatabaseId: "",
    list: [],
    isSettingsOpened: false,
    refreshSiteCount: 0,
    videosPerPage: REACT_APP_DEFAULT_VIDEO_PER_PAGE,
    orderedByAsc: 1,
  },
  reducers: {
    setSelectedDatabaseId: (state, action) => {
      state.selectedDatabaseId = action.payload;
    },
    setIsSettingsOpened: (state, action) => {
      state.isSettingsOpened = action.payload;
    },
    setVideosPerPage: (state, action) => {
      state.videosPerPage = action.payload;
    },
    setList: (state, action) => {
      state.list = action.payload;
    },
    removeFromList: (state, action) => {
      const databaseId = action.payload;
      const index = state.list.findIndex(
        (option) => String(option.databaseId) === String(databaseId)
      );
      if (index !== -1) state.list.splice(index, 1);
    },
    addToList: (state, action) => {
      state.list = state.list.concat(action.payload);
    },
    triggerRefreshSite: (state, action) => {
      state.refreshSiteCount++;
    },
    setOrderedByAsc: (state, action) => {
      state.orderedByAsc = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  setSelectedDatabaseId,
  setIsSettingsOpened,
  setList,
  addToList,
  removeFromList,
  triggerRefreshSite,
  setVideosPerPage,
  setOrderedByAsc,
} = settingSlice.actions;

export default settingSlice.reducer;
