import { createSlice } from "@reduxjs/toolkit";

export const databaseVideoSlice = createSlice({
  name: "databaseVideo",
  initialState: {
    originalVideos: [],
  },
  reducers: {
    setOriginalVideos: (state, action) => {
      state.originalVideos = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setOriginalVideos } = databaseVideoSlice.actions;

export default databaseVideoSlice.reducer;
