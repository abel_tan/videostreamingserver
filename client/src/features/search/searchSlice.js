import { createSlice } from "@reduxjs/toolkit";

const defaultValues = {
  criteria: { title: "*", label: "*", like: "*" },
};
export const searchSlice = createSlice({
  name: "search",
  initialState: defaultValues,
  reducers: {
    reset: () => defaultValues,
    updateCriteria: (state, action) => {
      state.criteria = { ...state.criteria, ...action.payload };
    },
  },
});

// Action creators are generated for each case reducer function
export const { updateCriteria, reset } = searchSlice.actions;

export default searchSlice.reducer;
