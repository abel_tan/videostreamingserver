import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import store from "./configs/store";
import { Provider } from "react-redux";
import MainPage from "./MainPage";

// As of React 18
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <MainPage />
    </Provider>
  </React.StrictMode>
);
