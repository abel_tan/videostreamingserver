import dayjs from "dayjs";

export function doSortById(
  videos = [],
  idName = "oldestBirthDate",
  orderByAsc = true
) {
  // return default videos;
  if (videos.length === 0) return videos;
  // sort listing
  const sortedVideos = [...videos].sort(
    (a, b) => dayjs(b[idName]) - dayjs(a[idName])
  );

  if (Boolean(orderByAsc)) {
    return sortedVideos;
  } else {
    return sortedVideos.reverse();
  }
}
